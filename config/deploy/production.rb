# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

# server 'example.com', user: 'deploy', roles: %w{app db web}, my_property: :my_value
# server 'example.com', user: 'deploy', roles: %w{app web}, other_property: :other_value
# server 'db.example.com', user: 'deploy', roles: %w{db}



# role-based syntax
# ==================

# Defines a role with one or multiple servers. The primary server in each
# group is considered to be the first unless any  hosts have the primary
# property set. Specify the username and a domain or IP for the server.
# Don't use `:all`, it's a meta role.

# role :app, %w{deploy@example.com}, my_property: :my_value
# role :web, %w{user1@primary.com user2@additional.com}, other_property: :other_value
# role :db,  %w{deploy@example.com}



# Configuration
# =============
# You can set any configuration variable like in config/deploy.rb
# These variables are then only loaded and set in this stage.
# For available Capistrano configuration variables see the documentation page.
# http://capistranorb.com/documentation/getting-started/configuration/
# Feel free to add new variables to customise your setup.



# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult the Net::SSH documentation.
# http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# The server-based syntax can be used to override options:
# ------------------------------------
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }

server 'irisdev-peerlogic.corp.ooma.com', port: 22, roles: [:web, :app, :db,:resque_worker,:resque_scheduler], primary: true
 
 

#role :resque_worker, :app
#role :resque_scheduler, :app

set :linked_dirs, %w{log public/for_devs public/videos }

#namespace :puma do


#before :start, :make_dirs
#end

#namespace :deploy do
#  desc "Make sure local git is in sync with remote."
#  task :check_revision do
#    on roles(:app) do
#      unless `git rev-parse HEAD` == `git rev-parse origin/master`
#        puts "WARNING: HEAD is not the same as origin/master"
#        puts "Run `git push` to sync changes."
#       exit
#      end
#    end
# end

#  desc 'Initial Deploy'
#  task :initial do
#    on roles(:app) do
#      before 'deploy:restart', 'puma:start'
#      invoke 'deploy'
#    end
#  end

#  desc 'Restart application'
#  task :restart do
#    on roles(:app), in: :sequence, wait: 5 do
#      invoke 'puma:restart'
#    end
# end

#sudo ln -nfs "/var/www/appname/current/config/nginx.conf" "/etc/nginx/sites-enabled/appname"

desc 'nginx symbolic link to current site'
task :nginxsymboliclink do
  on roles(:app), in: :sequence, wait: 5 do
    execute("sudo unlink /etc/nginx/sites-enabled/iris_production")
    execute("sudo ln -nfs  #{release_path}/config/deploy/templates/iris_production /etc/nginx/sites-enabled/iris_production")
  end
end




desc 'start nginx'
task :startnginx do
  on roles(:app), in: :sequence, wait: 5 do
    execute("sudo systemctl start nginx.service")
  end
end


desc 'stop nginx'
task :stopnginx do
  on roles(:app), in: :sequence, wait: 5 do
    execute("sudo systemctl stop nginx.service")
  end
end


desc 'Create a symbolic link for envoronment variable like secret_key used in secret.yml'
task :envsymlink do
  on roles(:app), in: :sequence, wait: 5 do
    execute "ln -nfs #{shared_path}/.env #{release_path}/.env"
    execute "ln -nfs #{shared_path}/grafana_images #{release_path}/public/images/grafana_images"
  end
end



desc 'copy puma.sh script file to start and restart the puma web server'
task :copypumascript do
  on roles(:app), in: :sequence, wait: 5 do
    execute "cp #{release_path}/puma.sh #{shared_path}/"
    execute "cp #{release_path}/puma.rb #{shared_path}/"
    execute "chmod 777 #{shared_path}/puma.sh"
    execute "chmod 777 #{shared_path}/puma.rb"
  end
end

# desc 'create a shared log folder so when deployed at that time this folder will not change'
#   task :sharedlog do
#     on roles(:app), in: :sequence, wait: 5 do

#              execute "cd #{shared_path} && sh log.sh"

#     end
#   end



#before :starting,     :check_revision
#after  "bundler:install",   "database_yml:setup"
#after  "bundler:install",   "secrets_yml:setup"

#after  "bundler:install",   "envsymlink"
#after  "bundler:install",   "copypumascript"


namespace :deploy do
  before :starting, :make_dirs do
    on roles(:app) do
      latestrevision = capture("cat #{current_path}/REVISION")
      `git pull origin iris-p4`
      lastcommit = `git rev-parse HEAD`
      latestrevision = latestrevision.strip
      lastcommit = lastcommit.strip
      
      if latestrevision == lastcommit
        p "currently the latest release is already installed so no need to deploy this"
        exit
      else
        p "not same continue on deploy"
      end
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end




  after :finishing, :startpuma do
    on roles(:app), in: :sequence, wait: 5 do
      #execute "cd /export/iris/iris_production/shared/ && ./puma.sh stop"
      #execute "cd /export/iris/iris_production/shared/ &&  ./puma.sh start"
      p "now first stop monit service"
      execute "sudo systemctl stop monit"
      p "stopped monit"
      p "after this kill all old puma threads Jeff"
      #execute "ps auxww | grep puma | grep -v grep | awk '{print $2}' | xargs -n 1 -i kill -9 {}"
      sleep 5
      execute "cd /export/iris/iris_production/shared/ &&  ./puma.sh restart"
      # instead of phased restart let it be stop and start as there may be 
      # a problem with phased or normal puma restart that old workers and master 
      # process not get killed therefore in stop i am killing old 
      # workers and in start it will normally start 
      #execute "cd /export/iris/iris_production/shared/ && ./puma.sh stop"
      #execute "cd /export/iris/iris_production/shared/ &&  ./puma.sh start"
      p "restarted puma"
      sleep 5 
      execute "sudo systemctl  start monit"
      p "started monit service"
    end
  end



  after :finishing, :deploy_iris_mirror do
    on roles(:app), in: :sequence, wait: 5 do
        system("hostname")
        system("pwd")
        system("cap development deploy")
    end
  end


end

# before  "bundler:install",   "sharedlog"

#after  :finishing,    :compile_assets
#after  :finishing,    :cleanup
#after  :finish,    :startpuma
#end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma

#steps to run deployment
#cap production deploy


#set :workers, { "host_info_loop,host_details_loop" => 8,"database_loop"=>6 ,"analyze_rpm"=>1,"kazoo_sup_test_results"=>1,"purge_expired_test_results"=>1,"voxweaver_test_results"=>1,"full_voxweaver_data"=>1,"node_data"=>1 }
#role :resque_worker, "graphite.corp.ooma.com"
#role :resque_scheduler, "graphite.corp.ooma.com"



desc 'restart puma without deploy '
task :restartpuma do
  on roles(:app), in: :sequence, wait: 5 do
    execute "cd /export/iris/iris_production/shared/ && ./puma.sh stop"
    execute "cd /export/iris/iris_production/shared/ &&  ./puma.sh start"
  end
end

desc 'Create Grafana Images'
task :grafana_images do
  on roles(:app), in: :sequence, wait: 5 do
    execute "mkdir #{shared_path}/grafana_images -p"
  end
end

desc 'Switch current project into maintenance mode'
task :lock do
  on roles(:app), in: :sequence, wait: 5 do
    execute  "touch #{shared_path}/public/system/maintenancedev.html"
    execute  "mv #{shared_path}/public/system/maintenancedev.html  #{shared_path}/public/system/maintenance.html"
  end
end

desc 'Turn off current project maintenance mode'
task :unlock do
  on roles(:app), in: :sequence, wait: 5 do
    execute " mv #{shared_path}/public/system/maintenance.html  #{shared_path}/public/system/maintenancedev.html"
  end
end
